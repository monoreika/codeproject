#include <iostream>
#include "Helpers.h"
#include <string>
#include <iomanip>
#include <time.h>
#include <math.h>
using namespace std;

//int second()
//{
//    int a = 0;
//
//    switch (a)
//    {
//    case 0:
//        std::cout << "switch case 0";
//        break;
//    case 1:
//        std::cout << "switch case 1";
//        break;
//    default:
//        std::cout << "switch case default";
//        break;
//    }
//    return 0;
//}
//
//int third()
//{
//    int b = 10;
//    while (b < 10)
//    {
//        b++;
//        std::cout << "HEY JOHNY";
//    }
//    return b;
//}
//
//int name()
//{
//    if (SumSquare(2, 3) != 5)
//    {
//        std::cout << "Enter your name, please" << " ";
//        std::string name;
//        std::getline(std::cin, name);
//        std::cout << "\n";
//
//        std::cout << "Name: " << name << "\n";
//        std::cout << "Number of characters in the name: " << name.length() << "\n";
//        std::cout << "First letter: " << name.front() << "\n";
//        std::cout << "Last letter: " << name.back() << "\n";
//    }
//    else
//    {
//        std::cout << "HI DEAR FELLA";
//    }
//}
//void EvenOrOdd(const int limit = 10, bool Odd = true) //���� ��������� ���������� "Odd" �������� "false" �� ������� ��������
//{
//    if (Odd == true) //���� �������� - ���, �� ��������� ����, ������� ������� ������
//    {
//        for (int i = 0; i < limit; i++)
//        {
//            if (i % 2 == 0)
//            {
//                std::cout << i << "\n";
//            }
//        }
//    }
//    else //����� ��������� ����, ������� ������� ��������
//    {
//        for (int i = 0; i < limit; i++)
//        {
//            if (i % 2 != 0)
//            {
//                std::cout << i << "\n";
//            }
//        }
//    }
//}
//const int N = 10;
//for (int i = 0; i < N; i++)
//{
//
//    if (i % 2 == 0)
//    {
//        std::cout << i << "\n";
//    }
//
//}
//std::cout << "\n" << "now i will show you array" << "\n" << "\n";
//
//const int size = 10;
//int array[size] = { 1,2,3,4,5,6,7,8,9,10 };
//
//for (int i = 0; i < size; i++)
//{
//    std::cout << array[i];
//}

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void ShowVectorOrigin()
	{
		cout << "\n" << "original vector value" << "\n";
		cout << "x = " << x << " y = " << y << " z = " << z << "\n";
	};


	void ShowVectorMod()
	{
		cout << "\n" << "vector value module" << "\n";
		cout << "|x| = " << abs(x) << " |y| = " << abs(y) << " |z| = " << abs(z) << "\n";
	};

private:

	double x;
	double y;
	double z;

};

int main()
{
	Vector v(5, -5, 10);
	v.ShowVectorOrigin();
	v.ShowVectorMod();
}